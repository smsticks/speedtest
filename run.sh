#!/bin/bash

# Set your virtual environment name here
virtenv_name="stickeldev"

# Activates Virtual Environment
#echo "source ~/.virtualenvs/"$virtenv_name"/bin/activate"
source ~/.virtualenvs/"$virtenv_name"/bin/activate

# Gets the current path to python script
local_path=$( dirname "${BASH_SOURCE[0]}" ) && pwd
echo "local_path:" $local_path

# Goto directory of project
cd $local_path

#
# Execute Speed Test
#
echo "python test_speed.py"
python test_speed.py
