# SpeedTest
Python script to run and compare home internet speeds and to alert home owner if the speed slows down below what they're paying for.

You'll need my library to run this.

- https://bitbucket.org/smsticks/stickel

or with pip:
- sudo pip install -e git+https://smsticks@bitbucket.org/smsticks/stickel.git#egg=Stickel