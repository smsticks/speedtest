# -*- coding: utf-8 -*-
import stickel
import pyspeedtest
import os

__author__ = 'Steve stickel'
__copyright__ = "Copyright 2016"
__license__ = "GPL"
__maintainer__ = "Steve stickel"
__email__ = "tdsticks@gmail.com"


class TestSpeed():
    def __init__(self, debug=None):
        self.debug = debug

        self.S = stickel

        self.log_name = "TestSpeed"

        self.g = self.S.Globals(debug=False)
        self.dateTime = self.S.dateTime.DateTime(debug=False)
        self.fu = self.S.fileUtility.FileUtility(debug=False)

        # Start the logger
        self.l = self.S.logger.Logger(debug=False)
        self.l.create_logger(self.log_name)
        self.l.init_log()

        self.l.log(1, "TestSpeed::__init__")

        # Override the logging
        pyspeedtest.LOG.setLevel(20)

        self.pst = pyspeedtest.SpeedTest(host=None, http_debug=0, runs=2)
        # self.pst = pyspeedtest.SpeedTest()

        get_datetime = self.dateTime.get_fmt_datetime()

        header_list = ["datetime", "download", "upload", "ping"]
        data_list = []

        data_list.append(get_datetime)
        data_list.append(self.test_download_speed())
        data_list.append(self.test_upload_speed())
        data_list.append(str(self.test_ping()) + "Seconds")
        print "data_list", data_list

        self.write_to_file(header_list, data_list)

    def test_download_speed(self):
        self.l.log(1, "TestSpeed::test_download_speed")

        return self.pretty_speed(self.pst.download())

    def test_upload_speed(self):
        self.l.log(1, "TestSpeed::test_upload_speed")

        return self.pretty_speed(self.pst.upload())

    def test_ping(self):
        self.l.log(1, "TestSpeed::test_ping")

        self.pst.ping()

        return round(self.pst.ping(), 1)

    def write_to_file(self, header, data):
        self.l.log(1, "TestSpeed::write_to_file")

        file_date = self.dateTime.get_fmt_date_for_file()
        file_name = self.log_name + file_date + ".csv"
        file_path = self.g.ntwk_path + "data" + self.g.ds + file_name
        # print file_path

        header_str = ""
        data_str = ""

        # check for the file, if new add headers
        if not os.path.isfile(file_path):
            header_str = (",").join(header) + "\n"

            # Write the headers to the file
            self.fu.write_file(file_path, header_str)
            # print "header_str",header_str

        # Add the data to the file
        data_str = (",").join(data) + "\n"
        # print "write_str", data_str

        # Write to the data to the file
        self.fu.write_file(file_path, data_str)

    def pretty_speed(self, speed):
        units = ['bps', 'Kbps', 'Mbps', 'Gbps']
        unit = 0
        while speed >= 1024:
            speed /= 1024
            unit += 1
        return '%0.2f %s' % (speed, units[unit])

if __name__ == '__main__':

    TestSpeed(debug=True)